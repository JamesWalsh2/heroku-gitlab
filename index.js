const express = require('express')
const app = express()

app.set('PORT', process.env.PORT || 3000)

app.get('/', (req, res) => {
    res.status(200).send({
        message: 'The module code is IS3313. My student number is 117390791.',
    })
})

app.listen(app.get('PORT'), () =>
    console.log(`Server running on port ${app.get('PORT')}`),
)
